package support;

import java.nio.CharBuffer;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TestParseSerial {
	
	static Logger logger = Logger.getLogger(TestParseSerial.class.getSimpleName());
	static int pitch = 0, roll = 0, yaw = 0;
	static int[] rc = new int[6];
	static CharBuffer buf = CharBuffer.allocate(100);
	
	public static void main(String[] args) {
		//String s = "AP=12|P=0|R=-5|Y=1|RC=1000:2000:3000:4000:5000:6000";
		//String s = "AP=12|P=0|R=-5|Y=1|RC=";
		//String data = "AP=-5|P=1572|R=-14209|Y=13687|RC=0:0:0:0:0:0AP=-5|P=1572|R=-14211|Y=13680|RC=0:0:0:0:0:0|AP=-5|P=1573|R=-14212|Y=13675|RC=0:0:0:0:0:0";
		//String data = "AP=-4|P=���I�u����e�u�����I������������j��AP=-5|P=221|R=-752|Y=-4024|RC=1:2:3:4:5:6|P=-1  \n";
		String data = "AP=5|P=-37|R=66|Y=-120|RC=111:222:333\n |DEBUG=Hello World  \n";
		System.out.println(data);
		parseMessage(data);
		
		System.out.println("pitch:" + pitch);
		System.out.println("roll:" + roll);
		System.out.println("yaw:" + yaw);
		
		System.out.println(Arrays.toString(rc));

	}
	
	public static void parseMessage(String in) {
		byte[] inByte = in.getBytes();
		for (int i =0; i < inByte.length; i++) {
			char c = (char)inByte[i];
			if (c == '\n' || c == '\r') {
				buf.rewind();
				String message = buf.toString().trim();
				splitByDelimiter(message);
				buf.clear();
			} else if (c >= 32 && c <= 125 && buf.hasRemaining()){
				buf.put(c);
				System.out.print(c);
			}
		}
	}
	
	public static void splitByDelimiter(String data) {
		if (data != null && data.startsWith("AP=")) {
			String[] da = data.split("\\|");
			for (String token : da) {
				String[] kv = token.split("=");
				if (kv.length == 2) {
					if (kv[0].startsWith("RC")) {
						String[] rcToken = kv[1].split(":");
						for (int i = 0; i < rcToken.length; i++) {
							try {
								rc[i] = Integer.parseInt(rcToken[i]);
							} catch (NumberFormatException e) {}
						}
					} else {
						if ("P".equals(kv[0])) {
							try {
							pitch = Integer.parseInt(kv[1]);
							} catch (NumberFormatException e) {}
						} else if ("R".equals(kv[0])) {
							try {
								roll = Integer.parseInt(kv[1]);
							} catch (NumberFormatException e) {}
						} else if ("Y".equals(kv[0])) {
							try {
								yaw = Integer.parseInt(kv[1]);
							} catch (NumberFormatException e) {}
						} else if ("AP".equals(kv[0])) {
						} else if ("DEBUG".equals(kv[0].trim())) {
							logger.info(kv[1]);
						} else {
							logger.log(Level.WARNING, "Unknown key " + kv[0]);
						}
					}
				} else {
					//logger.log(Level.WARNING, "Unknown serial message " + data);
				}
			}
		}
	}

}
