package support;

import static org.junit.Assert.*;

import org.junit.Test;

public class ValueMappingTest {
	
	@Test
	public void testBetweenPercent() {
		assertEquals(50, ValueMapping.mapValueToPercent(147, 98, 196));
		assertEquals(0, ValueMapping.mapValueToPercent(98, 98, 196));
		assertEquals(100, ValueMapping.mapValueToPercent(196, 98, 196));
		assertEquals(74, ValueMapping.mapValueToPercent(171, 98, 196));
		assertEquals(24, ValueMapping.mapValueToPercent(122, 98, 196));
	}
	
	@Test
	public void testPercentToValue() {
		assertEquals(100, ValueMapping.mapPercentToValue(100, 0, 100));
		assertEquals(50, ValueMapping.mapPercentToValue(50, 0, 100));
		assertEquals(50, ValueMapping.mapPercentToValue(50, 1, 100));
		
		assertEquals(50, ValueMapping.mapPercentToValue(100, 1, 50));
		assertEquals(1, ValueMapping.mapPercentToValue(0, 1, 50));
		assertEquals(25, ValueMapping.mapPercentToValue(50, 0, 50));
	}

	@Test
	public void testNotBetween() {
		//test not between
		assertFalse(ValueMapping.notBetween(49, 48, 2));
		assertTrue(ValueMapping.notBetween(40, 48, 2));
		assertTrue(ValueMapping.notBetween(56, 48, 2));
	}

}
