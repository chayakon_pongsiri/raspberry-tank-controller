package support;

public class GlobalState {

	public static boolean FRONT_LIGHT = true;
	public static boolean BACK_LIGHT = true;
	public static boolean ENGINE_STARTED = false;
	public static boolean SOUND_ENABLED = true;
	public static int SOUND_VOLUME = 100;
}
