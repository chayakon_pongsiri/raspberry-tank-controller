import controller.DefaultActionController;
import support.Config;

public class Application {
	
	static Config config = Config.getInstance();
	
	public static void main(String[] args) throws Exception {
		System.setProperty("java.util.logging.SimpleFormatter.format", 
	            "%1$tF %1$tT %4$s %2$s %5$s%6$s%n");
		
		config.load("config/application.properties");
		
		DefaultActionController controller = new DefaultActionController();
		controller.start();
		
		Runtime.getRuntime().addShutdownHook(new Thread() {
		    public void run() {
		    	System.out.println("Release resource");
		    	controller.stop();
		    }
		});
		
	}

}
