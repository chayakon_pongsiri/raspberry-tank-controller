package support;

public enum MotorDirection {

	CW, CCW, BREAK, NEUTRAL;
}
