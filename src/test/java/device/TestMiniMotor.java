package device;

import java.io.FileNotFoundException;
import java.io.IOException;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.RaspiPin;

import support.Config;
import support.MotorDirection;
import support.Timer;

public class TestMiniMotor {

	public static void main(String[] args) throws FileNotFoundException, IOException {
		System.setProperty("java.util.logging.SimpleFormatter.format", 
	            "%1$tF %1$tT %4$s %2$s %5$s%6$s%n");
		
		String dir = System.getProperty("dir");
		int speed = Integer.parseInt(System.getProperty("speed"));
		int sleep = Integer.parseInt(System.getProperty("sleep"));
		
		GpioController gpio = GpioFactory.getInstance();
		Config config = Config.getInstance();
		config.load("config/application.properties");
		Mini2WayPwmMotorDriver motor = new Mini2WayPwmMotorDriver(
				RaspiPin.getPinByAddress(config.getInt("turret.motor.pwm1")), 
				RaspiPin.getPinByAddress(config.getInt("turret.motor.pwm2")));
		
		motor.start();
		System.out.println("Start");
		
		if ("CW".equals(dir)) {
			System.out.println("CW");
			motor.setDirectionA(MotorDirection.CW);
		} else {
			System.out.println("CCW");
			motor.setDirectionA(MotorDirection.CCW);
		}

		motor.setSpeedA(speed);
		Timer.sleep(sleep);

		
		motor.stop();
		System.out.println("Stop");
		gpio.shutdown();
		System.out.println("shutdown");
		System.exit(0);
	}

}
