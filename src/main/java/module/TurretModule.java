package module;

import java.io.IOException;
import java.util.logging.Logger;

import com.pi4j.io.gpio.RaspiPin;

import device.Mini2WayPwmMotorDriver;
import support.Config;
import support.MotorDirection;
import support.Timer;

public class TurretModule {

	Logger logger = Logger.getLogger(TurretModule.class.getSimpleName());
	Config config = Config.getInstance();
	
	int pitch, roll, yaw;
	int delay = 50;
	boolean stabilizerEnabled = false;
	Mini2WayPwmMotorDriver motor;
	Thread turretThread;
	
	public TurretModule() {
		delay = config.getInt("delay", "50");
		motor = new Mini2WayPwmMotorDriver(
				RaspiPin.getPinByAddress(config.getInt("turret.motor.pwm1")), 
				RaspiPin.getPinByAddress(config.getInt("turret.motor.pwm2")));
	}
	
	public void start() throws IOException {
		motor.start();
		turretThread = new Thread(()->{
			
			while(true) {
				int diff = pitch;
				if (diff > 1000) {
					motor.setSpeedA(100);
					motor.setDirectionA(MotorDirection.CCW);
					logger.info("Turret turn CCW " + 100);
				} else if (diff > 500) {
					motor.setSpeedA(70);
					motor.setDirectionA(MotorDirection.CCW);
					logger.info("Turret turn CCW " + 50);
				} else if (diff > 50) {
					motor.setSpeedA(30);
					motor.setDirectionA(MotorDirection.CCW);
					logger.info("Turret turn CCW " + 30);
				} else if (diff > 20) {
					motor.setSpeedA(15);
					motor.setDirectionA(MotorDirection.CCW);
					logger.info("Turret turn CCW " + 15);
				} else if (diff < -1000) {
					motor.setSpeedA(90);
					motor.setDirectionA(MotorDirection.CW);
					logger.info("Turret turn CW " + 100);
				} else if (diff < -500) {
					motor.setSpeedA(70);
					motor.setDirectionA(MotorDirection.CW);
					//logger.info("Turret turn CW " + 50);
				} else if (diff < -50) {
					motor.setSpeedA(30);
					motor.setDirectionA(MotorDirection.CW);
					//logger.info("Turret turn CW " + 20);
				} else if (diff < -20) {
					motor.setSpeedA(15);
					motor.setDirectionA(MotorDirection.CW);
					//logger.info("Turret turn CW " + 15);
				} else {
					motor.idleA();
				}
				Timer.sleep(delay);
			}
		});
		turretThread.start();
		logger.info("Turret Started");
	}
	
	public void update(int pitch, int roll, int yaw) {
		this.pitch = pitch;
		this.roll = roll;
		this.yaw = yaw;
	}
	
	public void stop() {
		if (turretThread != null && turretThread.isAlive()) {
			turretThread.interrupt();
		}
		motor.stop();
	}
	
	public void toggleStabilizer() {
		//TODO
	}

	public void moveLeft(int speed) {
		if (!stabilizerEnabled) {
			return;
		}
		doMoveLeft(speed);
	}
	
	public void moveRight(int speed) {
		if (!stabilizerEnabled) {
			return;
		}
		doMoveRight(speed);
	}
	
	public void idleTurret() {
		
	}
	
	private void doMoveLeft(int speed) {
		motor.setSpeedA(speed);
		motor.setDirectionA(MotorDirection.CW);
	}
	
	private void doMoveRight(int speed) {
		motor.setSpeedA(speed);
		motor.setDirectionA(MotorDirection.CCW);
	}
	
	
}
