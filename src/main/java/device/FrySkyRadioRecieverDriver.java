package device;

import java.util.logging.Level;
import java.util.logging.Logger;

import jpigpio.JPigpio;
import jpigpio.Pigpio;
import jpigpio.Utils;
import support.Config;
import support.GpioLevelChangeListener;

public class FrySkyRadioRecieverDriver {

	Logger logger = Logger.getLogger(FrySkyRadioRecieverDriver.class.getSimpleName());
	Config config = Config.getInstance();
	JPigpio pigpio;
	GpioLevelChangeListener ch1;
	GpioLevelChangeListener ch2;
	GpioLevelChangeListener ch3;
	GpioLevelChangeListener ch4;
	GpioLevelChangeListener ch5;
	GpioLevelChangeListener ch6;
	
	public FrySkyRadioRecieverDriver() throws Exception {
		pigpio = new Pigpio();
		pigpio.gpioInitialize();
		ch1 = new GpioLevelChangeListener(pigpio, config.getInt("reciever.ch1.pin"));
		ch2 = new GpioLevelChangeListener(pigpio, config.getInt("reciever.ch2.pin"));
		ch3 = new GpioLevelChangeListener(pigpio, config.getInt("reciever.ch3.pin"));
		ch4 = new GpioLevelChangeListener(pigpio, config.getInt("reciever.ch4.pin"));
		ch5 = new GpioLevelChangeListener(pigpio, config.getInt("reciever.ch5.pin"));
		ch6 = new GpioLevelChangeListener(pigpio, config.getInt("reciever.ch6.pin"));
		Utils.addShutdown(pigpio);
	}
	
	public void start() throws Exception {
		logger.info("Initilize radio reciever");
		ch1.start();
		ch2.start();
		ch3.start();
		ch4.start();
		ch5.start();
		ch6.start();
		logger.info("Initilize radio reciever success");
	}
	
	public void stop() {
		try {
			ch1.stop();
			ch2.stop();
			ch3.stop();
			ch4.stop();
			ch5.stop();
			ch6.stop();
			pigpio.gpioTerminate();
			logger.info("Shutdown radio reciever success");
		} catch (Exception e) {
			logger.log(Level.WARNING, "Error during stop pigpio", e);
		}
	}

	public GpioLevelChangeListener getCh1() {
		return ch1;
	}

	public GpioLevelChangeListener getCh2() {
		return ch2;
	}

	public GpioLevelChangeListener getCh3() {
		return ch3;
	}

	public GpioLevelChangeListener getCh4() {
		return ch4;
	}

	public GpioLevelChangeListener getCh5() {
		return ch5;
	}

	public GpioLevelChangeListener getCh6() {
		return ch6;
	}
	
	
	
}
