package device;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.PinState;

import support.Config;
import support.MotorDirection;
import support.Timer;
import support.ValueMapping;

public class Bts7960MotorDriver {

	Logger logger = Logger.getLogger(Bts7960MotorDriver.class.getSimpleName());
	
	Config config = Config.getInstance();
	GpioController gpio = GpioFactory.getInstance();
	GpioPinDigitalOutput R_PWM, L_PWM;
	
	int[] frequencyValues = {0, 10, 13, 18, 21, 25, 29, 33, 36, 42, 47, 
			55, 70, 90, 120, 200, 300, 450, 600, 800, 1000};
	int frequency; //Hz
	int dutyCycle;
	
	MotorDirection direction;
	Thread thread;
	String name;
	boolean running;
	
	public Bts7960MotorDriver(String name, Pin R_PWM_PIN, Pin L_PWM_PIN) {
		this.name = name;
		this.dutyCycle = config.getInt("track.motor.duty", "3");
		
		R_PWM = gpio.provisionDigitalOutputPin(R_PWM_PIN, PinState.LOW);
		L_PWM = gpio.provisionDigitalOutputPin(L_PWM_PIN, PinState.LOW);
		R_PWM.setShutdownOptions(true, PinState.LOW);
		L_PWM.setShutdownOptions(true, PinState.LOW);
	}
	
	public MotorDirection getDirection() {
		return direction;
	}
	
	public void setDirection(MotorDirection direction) {
		this.direction = direction;
	}
	
	/**
	 * 
	 * @param speed between 0 - 100
	 */
	public void setSpeed(int speedPercent) {
		int index = ValueMapping.mapPercentToValue(speedPercent, 1, frequencyValues.length - 1);
		this.frequency = frequencyValues[index];
		//logger.info(this.name + ", speed:" + speedPercent + ", freq:" + this.frequency + "," + (int)(1000/frequency));
	}
	
	public void start() {
		thread = new Thread(()->{
			while(true) {
				try {
					if (frequency <= 0) {
						running = false;
						L_PWM.setState(PinState.LOW);
						R_PWM.setState(PinState.LOW);
						Timer.sleep(dutyCycle);
						continue;
					}
					
					if (direction == MotorDirection.CW) {
						running = true;
						L_PWM.setState(PinState.HIGH);
						R_PWM.setState(PinState.LOW);
					} else if (direction == MotorDirection.CCW){
						running = true;
						L_PWM.setState(PinState.LOW);
						R_PWM.setState(PinState.HIGH);
					} else if (direction == MotorDirection.BREAK){
						L_PWM.setState(PinState.HIGH);
						R_PWM.setState(PinState.HIGH);
						running = false;
					} else if (direction == MotorDirection.NEUTRAL){
						L_PWM.setState(PinState.LOW);
						R_PWM.setState(PinState.LOW);
						running = false;
					}

					Timer.sleep(dutyCycle);
					L_PWM.setState(PinState.LOW);
					R_PWM.setState(PinState.LOW);
					if (frequency > 0) {
						double sleep = 1000.0/frequency;
						long microsleep = (long)sleep;
						int nanosleep = (int)(sleep - microsleep) *100000;
						Timer.sleep(microsleep, nanosleep);
					}
				} catch (Exception e) {
					logger.log(Level.SEVERE, "Error during control track motor", e);
					break;
				}
			}
		});
		thread.start();
		
	}
	
	public void stop() {
		if (thread != null && thread.isAlive()) {
			thread.interrupt();
		}
		L_PWM.setState(PinState.LOW);
		R_PWM.setState(PinState.LOW);
	}
	
	public boolean isRunning() {
		return running;
	}
}
