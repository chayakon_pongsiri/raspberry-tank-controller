package module;

import com.pi4j.io.gpio.RaspiPin;

import device.Bts7960MotorDriver;
import support.Config;
import support.MotorDirection;
import support.Timer;
import support.ValueMapping;

public class TrackModule {
	
	Config config = Config.getInstance();
	Bts7960MotorDriver left;
	Bts7960MotorDriver right;
	
	public TrackModule()  {
		left = new Bts7960MotorDriver("left", 
				RaspiPin.getPinByAddress(config.getInt("track.motor.left.r_pwm")), 
				RaspiPin.getPinByAddress(config.getInt("track.motor.left.l_pwm")));
		
		right = new Bts7960MotorDriver("right", 
				RaspiPin.getPinByAddress(config.getInt("track.motor.right.r_pwm")), 
				RaspiPin.getPinByAddress(config.getInt("track.motor.right.l_pwm")));
	}
	
	public void start() {
		left.start();
		right.start();
	}
	
	public void stop() {
		left.stop();
		right.stop();
	}
	
	public void forward(int speed) {
		left.setDirection(MotorDirection.CW);
		left.setSpeed(speed);
		right.setDirection(MotorDirection.CW);
		right.setSpeed(speed);
	}
	
	public void forwardLeft(int speed, int turnSpeed) {
		int i = 0;
		int turnPercent = 0;
		if (turnSpeed <= 50) {
			i = turnSpeed;
			left.setDirection(MotorDirection.CW);
			turnPercent = ValueMapping.mapValueToPercent(i, 0, 50);
			turnPercent = (int)(speed * (1 - (turnPercent/100.0)));
		} else {
			i = turnSpeed - 50;
			left.setDirection(MotorDirection.CCW);
			turnPercent = ValueMapping.mapValueToPercent(i, 0, 50);
			turnPercent = (int)(speed * (turnPercent/100.0));
		}
		left.setSpeed(turnPercent);
		
		right.setDirection(MotorDirection.CW);
		right.setSpeed(speed);
	}
	
	public void forwardRight(int speed, int turnSpeed) {
		int i = 0;
		int turnPercent = 0;
		if (turnSpeed <= 50) {
			i = turnSpeed;
			right.setDirection(MotorDirection.CW);
			turnPercent = ValueMapping.mapValueToPercent(i, 0, 50);
			turnPercent = (int)(speed * (1 - (turnPercent/100.0)));
		} else {
			i = turnSpeed - 50;
			right.setDirection(MotorDirection.CCW);
			turnPercent = ValueMapping.mapValueToPercent(i, 0, 50);
			turnPercent = (int)(speed * (turnPercent/100.0));
		}
		right.setSpeed(turnPercent);
		
		left.setDirection(MotorDirection.CW);
		left.setSpeed(speed);
	}
	
	public void backwardLeft(int speed, int turnSpeed) {
		int i = 0;
		int turnPercent = 0;
		if (turnSpeed <= 50) {
			i = turnSpeed;
			left.setDirection(MotorDirection.CCW);
			turnPercent = ValueMapping.mapValueToPercent(i, 0, 50);
			turnPercent = (int)(speed * (1 - (turnPercent/100.0)));
		} else {
			i = turnSpeed - 50;
			left.setDirection(MotorDirection.CW);
			turnPercent = ValueMapping.mapValueToPercent(i, 0, 50);
			turnPercent = (int)(speed * (turnPercent/100.0));
		}
		left.setSpeed(turnPercent);
		
		right.setDirection(MotorDirection.CCW);
		right.setSpeed(speed);
	}
	
	public void backwardRight(int speed, int turnSpeed) {
		int i = 0;
		int turnPercent = 0;
		if (turnSpeed <= 50) {
			i = turnSpeed;
			right.setDirection(MotorDirection.CCW);
			turnPercent = ValueMapping.mapValueToPercent(i, 0, 50);
			turnPercent = (int)(speed * (1 - (turnPercent/100.0)));
		} else {
			i = turnSpeed - 50;
			right.setDirection(MotorDirection.CW);
			turnPercent = ValueMapping.mapValueToPercent(i, 0, 50);
			turnPercent = (int)(speed * (turnPercent/100.0));
		}
		right.setSpeed(turnPercent);
		
		left.setDirection(MotorDirection.CCW);
		left.setSpeed(speed);
	}
	
	public void backward(int speed) {
		left.setDirection(MotorDirection.CCW);
		left.setSpeed(speed);
		right.setDirection(MotorDirection.CCW);
		right.setSpeed(speed);
	}
	
	public void neatral() {
		left.setDirection(MotorDirection.NEUTRAL);
		right.setDirection(MotorDirection.NEUTRAL);
	}
	
	public void engineBreak() {
		left.setDirection(MotorDirection.BREAK);
		right.setDirection(MotorDirection.BREAK);
	}
	
	public void cannonReaction() {
		if (left.isRunning() || right.isRunning()) {
			//no action when running
			return;
		}
		int speed = 100;
		int sleep = 50;
		
		neatral();
		left.setDirection(MotorDirection.CCW);
		left.setSpeed(speed);
		
		right.setDirection(MotorDirection.CCW);
		right.setSpeed(speed);
		Timer.sleep(sleep);
		neatral();
		Timer.sleep(1500);
	}
}
