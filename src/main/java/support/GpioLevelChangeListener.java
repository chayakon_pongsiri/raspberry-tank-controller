package support;
import jpigpio.Alert;
import jpigpio.JPigpio;

public class GpioLevelChangeListener implements Alert {

	long start;
	long end;
	int pin;
	int valueFiltered;
	JPigpio pigpio;
	
	public GpioLevelChangeListener(JPigpio pigpio, int pin) throws Exception {
		this.pin = pin;
		this.pigpio = pigpio;
		this.pigpio.gpioSetMode(pin, JPigpio.PI_INPUT);
		
	}
	
	public void start() throws Exception {
		this.pigpio.gpioSetAlertFunc(pin, this);
	}
	
	public void stop() throws Exception {
		
	}
	
	@Override
	public void alert(int gpio, int level, long tick) {
		if (level == 1) {
    		start = tick;
    	} else {
    		end = tick;
    	}
    	long diff = end - start;
    	if (diff >= 900 && diff <= 2100) {
    		valueFiltered = (int)(valueFiltered*0.50 + diff*0.50);
    	}
    	
    	Timer.sleep(5);
	}

	public int getValue() {
		return valueFiltered;
	}

	public int getPin() {
		return pin;
	}

	
}
