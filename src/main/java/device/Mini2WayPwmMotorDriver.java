package device;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.PinState;

import support.Config;
import support.MotorDirection;
import support.Timer;
import support.ValueMapping;

public class Mini2WayPwmMotorDriver {

	Logger logger = Logger.getLogger(Mini2WayPwmMotorDriver.class.getSimpleName());
	Config config = Config.getInstance();
	GpioController gpio = GpioFactory.getInstance();
	
	GpioPinDigitalOutput PORT_IN_1;
	GpioPinDigitalOutput PORT_IN_2;
	
	//int[] frequencyAArrays = {80, 90, 100, 150, 300};
	int[] frequencyAArrays = {60, 90, 150, 300, 900, 1200, 1600, 2000};
	//int[] frequencyAArrays = {300, 400, 600, 900, 1200, 2000};
	int frequencyA; //Hz
	int dutyCycle;
	
	MotorDirection directionA;
	Thread threadA;
	
	public Mini2WayPwmMotorDriver(Pin in1, Pin in2) {
		this.dutyCycle = config.getInt("turret.motor.duty", "2");
		
		PORT_IN_1 = gpio.provisionDigitalOutputPin(in1, PinState.LOW);
		PORT_IN_2 = gpio.provisionDigitalOutputPin(in2, PinState.LOW);
		PORT_IN_1.setShutdownOptions(true, PinState.LOW);
		PORT_IN_2.setShutdownOptions(true, PinState.LOW);
	}
	
	/**
	 * 
	 * @param speed between 0 - 100
	 */
	public void setSpeedA(int speedPercent) {
		int index = ValueMapping.mapPercentToValue(speedPercent, 0, frequencyAArrays.length - 1);
		this.frequencyA = frequencyAArrays[index];
		//logger.info("Set speed:" + speedPercent + ", freq:" + frequencyA);
	}
	
	public void idleA() {
		this.frequencyA = 0;
	}
	
	public void setDirectionA(MotorDirection direction) {
		this.directionA = direction;
	}
	
	public void start() {
		threadA = new Thread(()->{
			loopA();
		});
		threadA.start();
	}
	
	public void loopA() {
		while(true) {
			try {
				if (frequencyA <= 0) {
					PORT_IN_1.setState(PinState.LOW);
					PORT_IN_2.setState(PinState.LOW);
					Timer.sleep(dutyCycle);
					continue;
				}
				
				if (directionA == MotorDirection.CW) {
					PORT_IN_1.setState(PinState.HIGH);
					PORT_IN_2.setState(PinState.LOW);
				} else if (directionA == MotorDirection.CCW){
					PORT_IN_1.setState(PinState.LOW);
					PORT_IN_2.setState(PinState.HIGH);
				} else if (directionA == MotorDirection.BREAK){
					PORT_IN_1.setState(PinState.HIGH);
					PORT_IN_2.setState(PinState.HIGH);
				} else if (directionA == MotorDirection.NEUTRAL){
					PORT_IN_1.setState(PinState.LOW);
					PORT_IN_2.setState(PinState.LOW);
				}

				Timer.sleep(dutyCycle);
				PORT_IN_1.setState(PinState.LOW);
				PORT_IN_2.setState(PinState.LOW);
				
				if (frequencyA > 0) {
					double sleep = 1000.0/frequencyA;
					long microsleep = (long)sleep;
					int nanosleep = (int)(sleep - microsleep) *100000;
					Timer.sleep(microsleep, nanosleep);
				}
			} catch (Exception e) {
				logger.log(Level.SEVERE, "Error during set speed", e);
				break;
			}
		}
	}
	
	public void stop() {
		if (threadA != null && threadA.isAlive()) {
			threadA.interrupt();
		}
		PORT_IN_1.setState(PinState.LOW);
		PORT_IN_2.setState(PinState.LOW);
	}
}
