package module;

import support.GlobalState;

public class LightModule {
	
	public void toggleFrontLight() {
		if (GlobalState.FRONT_LIGHT) {
			GlobalState.FRONT_LIGHT = false;
		} else {
			GlobalState.FRONT_LIGHT = true;
		}
	}
	
	public void toggleBackLight() {
		if (GlobalState.BACK_LIGHT) {
			GlobalState.BACK_LIGHT = false;
		} else {
			GlobalState.BACK_LIGHT = true;
		}
	}
	
}
