package support;

public class ValueMapping {

	/**
	 * Map a current value to into percent
	 * @param current
	 * @param min
	 * @param max
	 * @return 0 - 100
	 */
	public static int mapValueToPercent(double current, double min, double max) {
		return (int)(Math.rint((current - min)/(max - min)*100));
	}
	
	public static int mapPercentToValue(double currentPercent, double min, double max) {
		if (currentPercent <= 0) {
			return (int)min;
		}
		if (currentPercent >= 100) {
			return (int)max;
		}
		return (int)(min + ((max - min) * currentPercent / 100));
	}
	
	public static boolean between(double val, double ref, double error) {
		return val >= ref - error && val <= ref + error;
	}
	
	public static boolean notBetween(double val, double ref, double error) {
		return val < ref - error || val > ref + error;
	}
	
	public static boolean moreThan(int val, int ref) {
		return val > ref;
	}
	
	public static boolean lessThan(int val, int ref) {
		return val < ref;
	}
		
}
