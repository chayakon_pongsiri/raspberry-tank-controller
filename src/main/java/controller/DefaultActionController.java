package controller;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import action.Action;
import device.ArduinoSensorDriver;
import device.FrySkyRadioRecieverDriver;
import module.TrackModule;
import module.TurretModule;
import support.Config;
import support.JoyStick;
import support.Timer;

public class DefaultActionController {
	
	Logger logger = Logger.getLogger(DefaultActionController.class.getSimpleName());
	
	Config config = Config.getInstance();
	
	JoyStick joy1;
	JoyStick joy2;

	Thread updateJoyStickThread;
	Thread updateDriveTrackThread;
	Thread updateGunAndLightThread;
	
	boolean connected = false;
	boolean engineStarted = false;
	
	TrackModule track;
	TurretModule turret;
	ArduinoSensorDriver arduinoRcAndSensorDriver;
	FrySkyRadioRecieverDriver radioRecieverDriver;
	
	int delay = 50;
	boolean started = false;
	
	public DefaultActionController() throws Exception {
		delay = config.getInt("delay", "50");
		int error = config.getInt("reciever.error");
		joy1 = new JoyStick(config.getInt("reciever.ch1.min"), config.getInt("reciever.ch1.max"), 
				config.getInt("reciever.ch2.min"), config.getInt("reciever.ch2.max"), 
				error);
		joy2 = new JoyStick(config.getInt("reciever.ch4.min"), config.getInt("reciever.ch4.max"), 
				config.getInt("reciever.ch3.min"), config.getInt("reciever.ch3.max"), 
				error);
		track = new TrackModule();
		turret = new TurretModule();
		
		radioRecieverDriver = new FrySkyRadioRecieverDriver();
		arduinoRcAndSensorDriver = new ArduinoSensorDriver();
	}
	
	public void start() throws Exception {
		updateJoyStickThread = new Thread(() -> {
			while(true) {
				updateJoyStickAndSensor();
				try {Thread.sleep(delay);} catch (Exception e) {break;}
			}
		});
		updateDriveTrackThread = new Thread(() -> {
			while(true) {
				updateDriveTrack();
				try {Thread.sleep(delay);} catch (Exception e) {break;}
			}
		});
		
		updateGunAndLightThread = new Thread(() -> {
			while(true) {
				updateGunAndLight();
				try {Thread.sleep(delay);} catch (Exception e) {break;}
			}
		});
		
		
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				while(true) {
					
					logger.info("P:" + arduinoRcAndSensorDriver.getPitch() 
						+ ",R:" + arduinoRcAndSensorDriver.getRoll()
						+ ",Y:" + arduinoRcAndSensorDriver.getYaw()
						+ ",1:" + radioRecieverDriver.getCh1().getValue() 
						+ ",2:" + radioRecieverDriver.getCh2().getValue()
						+ ",3:" + radioRecieverDriver.getCh3().getValue()
						+ ",4:" + radioRecieverDriver.getCh4().getValue()
						+ ",5:" + radioRecieverDriver.getCh5().getValue()
						+ ",6:" + radioRecieverDriver.getCh6().getValue()
						);
				
					Timer.sleep(100);
				}
				
			}
		}).start();
		
		updateJoyStickThread.start();
		updateDriveTrackThread.start();
		updateGunAndLightThread.start();
		
		track.start();
		turret.start();
		arduinoRcAndSensorDriver.start();
		radioRecieverDriver.start();
		started = true;
	}
	
	public void stop() {
		if (updateJoyStickThread != null && updateJoyStickThread.isAlive()) {
			updateJoyStickThread.interrupt();
		}
		if (updateDriveTrackThread != null && updateDriveTrackThread.isAlive()) {
			updateDriveTrackThread.interrupt();
		}
		if (updateGunAndLightThread != null && updateGunAndLightThread.isAlive()) {
			updateGunAndLightThread.interrupt();
		}
		
		
		// Cleanup
		started = false;
		track.stop();
		turret.stop();
		arduinoRcAndSensorDriver.stop();
		radioRecieverDriver.stop();
		
	}
	
	private void updateJoyStickAndSensor() {
		int v1 = radioRecieverDriver.getCh1().getValue();
		int v2 = radioRecieverDriver.getCh2().getValue();
		int v3 = radioRecieverDriver.getCh3().getValue();
		int v4 = radioRecieverDriver.getCh4().getValue();
		
		turret.update(
				arduinoRcAndSensorDriver.getPitch(), 
				arduinoRcAndSensorDriver.getRoll(), 
				arduinoRcAndSensorDriver.getYaw());
		
		if (v1 == 0 && v2 == 0 && v3 == 0 && v4 == 0) {
			logger.log(Level.WARNING, "RC Transmitter is not connect");
			Timer.sleep(2000);
			return;
		}
		
		joy1.update(v4, v3);
		joy2.update(v1, v2);
		
		if (joy1.isXYNearCenter() && joy2.isXYNearCenter()) {
			connected = true;//TODO break if running or nothing
		}
	}
	
	private void updateDriveTrack() {
		if (!connected) {
			return;
		}
		
		//Start engine
		if (!engineStarted && joy1.isYNearPercent(100) && joy2.isYNearPercent(100)) {
			logger.info("Start engine");
			Timer.sleep(2000);
			engineStarted = true;
			return;
		}
		
		//Stop engine
		if (engineStarted && joy1.isYNearPercent(0) && joy2.isYNearPercent(0)) {
			logger.info("Stop engine");
			track.neatral();
			Timer.sleep(2000);
			engineStarted = false;
			return;
		}
		
		if (!engineStarted) {
			return;
		}
		
		if (joy1.isXYNearCenter() && joy2.isXYNearCenter()) {
			track.neatral();
			return;
		}
		
		//Tank move forward and backward
		if  (joy1.isYMove()) {
			if (joy1.isMoveUp()) {
				int speed = joy1.getYPercentFromCenter();
				logger.info("Forward " + speed);
				track.forward(speed);
			} else if (joy1.isMoveDown()) {
				int speed = joy1.getYPercentFromCenter();
				logger.info("Backward " + speed);
				track.backward(speed);
			}
		}
		
		
		// Right and  Left turn
		if  (joy1.isYMove() && joy2.isXMove()) {
			if (joy1.isMoveUp()) {
				if (joy2.isMoveLeft()) {
					int speed = joy1.getYPercentFromCenter();
					int turnSpeed = joy2.getXPercentFromCenter();
					logger.info("Forward left:" + joy1.getYPercentFromCenter() + ",turnSpeed:" + turnSpeed);
					track.forwardLeft(speed, turnSpeed);
				} else if (joy2.isMoveRight()) {
					int speed = joy1.getYPercentFromCenter();
					int turnSpeed = joy2.getXPercentFromCenter();
					logger.info("Forward right:" + joy1.getYPercentFromCenter() + ",turnSpeed:" + turnSpeed);
					track.forwardRight(speed, turnSpeed);
				}
			} else if (joy1.isMoveDown()) {
				if (joy2.isMoveLeft()) {
					int speed = joy1.getYPercentFromCenter();
					int turnSpeed = joy2.getXPercentFromCenter();
					logger.info("Backward left:" + joy1.getYPercentFromCenter() + ",turnSpeed:" + turnSpeed);
					track.backwardLeft(speed, turnSpeed);
				} else if (joy2.isMoveRight()) {
					int speed = joy1.getYPercentFromCenter();
					int turnSpeed = joy2.getXPercentFromCenter();
					logger.info("Backward right:" + joy1.getYPercentFromCenter() + ",turnSpeed:" + turnSpeed);
					track.backwardRight(speed, turnSpeed);
				}
			}
		}
	}
	
	private void updateGunAndLight() {
		if (!connected) {
			return;
		}
		
		//TODO sound volume [ch5 vr move]
		//TODO cannon elevation/depression [ch6 vr move]
		
		if (joy1.isXYNearCenter() && joy2.isXYNearCenter()) {
			return;
		}
		
		//Booming Cannon
		if (joy1.isXNearPercent(0) && joy2.isYNearPercent(100)) {
			logger.info("Booming Cannon");
			track.cannonReaction();
			return;
		}
		
		//Gun Barrel Stabilizer on/off
		if (joy1.isXNearPercent(100) && joy2.isYNearPercent(100)) {
			logger.info("Gun Barrel Stabilizer on/off");
			turret.toggleStabilizer();
			Timer.sleep(1000);
			return;
		}
		
		//Cannon elevation
		//TODO may use ch5
		if (joy1.isXMove() && joy2.isYNearPercent(75)) {
			if (joy1.isMoveLeft()) {
				System.out.println(new Action("Cannon elevation down", joy1.getXPercentFromCenter(), 0));
				return;
			} else if (joy1.isMoveRight()) {
				System.out.println(new Action("Cannon elevation up",joy1.getXPercentFromCenter(), 0));
				return;
			}
		}
		
		//Turret rotation
		if (joy1.isXMove() && joy2.isYNearCenter()) {
			if (joy1.isMoveLeft()) {
				System.out.println( new Action("Turret rotation left", joy1.getXPercentFromCenter(), 0));
				turret.moveLeft(joy1.getXPercentFromCenter());
				Timer.sleep(500);
				return;
			} else if (joy1.isMoveRight()) {
				System.out.println( new Action("Turret rotation right", joy1.getXPercentFromCenter(), 0));
				turret.moveRight(joy1.getXPercentFromCenter());
				Timer.sleep(500);
				return;
			}
		} else {
			turret.idleTurret();
		}
		
		
		//Engine sound on/off
		if (joy1.isXNearPercent(100) && joy2.isYNearPercent(25)) {
			System.out.println( new Action("Engine sound on/off", 1000));
			Timer.sleep(1000);
			return;
		}
		
		//Fire MG
		if (joy1.isXNearPercent(0) && joy2.isYNearPercent(0)) {
			System.out.println( new Action("Fire MG", 500));
			Timer.sleep(100);
			return;
		}
		
		//Head light control on/off
		if (joy1.isXNearPercent(100) && joy2.isYNearPercent(0)) {
			System.out.println( new Action("Head light control on/off", 1000));
			Timer.sleep(1000);
			return;
		}
	}
	
}
