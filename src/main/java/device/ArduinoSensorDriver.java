package device;

import java.io.IOException;
import java.nio.CharBuffer;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pi4j.io.serial.Baud;
import com.pi4j.io.serial.DataBits;
import com.pi4j.io.serial.FlowControl;
import com.pi4j.io.serial.Parity;
import com.pi4j.io.serial.Serial;
import com.pi4j.io.serial.SerialConfig;
import com.pi4j.io.serial.SerialDataEvent;
import com.pi4j.io.serial.SerialDataEventListener;
import com.pi4j.io.serial.SerialFactory;
import com.pi4j.io.serial.SerialPort;
import com.pi4j.io.serial.StopBits;

import controller.DefaultActionController;
import support.Config;

public class ArduinoSensorDriver {
	
	Logger logger = Logger.getLogger(DefaultActionController.class.getSimpleName());
	
	Config config = Config.getInstance();
	Serial serial = SerialFactory.createInstance();
	
	int pitch, roll, yaw;
	byte[] newline = new byte[]{10};
	CharBuffer buf = CharBuffer.allocate(100);
	//boolean ready = false;
	
	public void start() throws Exception {
		logger.info("Initialize ArduinoSensorDriver");
		
		serial.addListener(new SerialDataEventListener() {
            @Override
            public void dataReceived(SerialDataEvent event) {
                try {
                	String data = event.getAsciiString();
					System.out.println("############## " + data);
					parseMessage(data);
                } catch (IOException e) {
                	logger.log(Level.SEVERE, "Error during parse serial data", e);
                }
            }
        });
		
		SerialConfig serialConfig = new SerialConfig();
		serialConfig.device(SerialPort.getDefaultPort())
	        .baud(Baud._38400)
	        .dataBits(DataBits._8)
	        .parity(Parity.NONE)
	        .stopBits(StopBits._1)
	        .flowControl(FlowControl.NONE);
		
		serial.open(serialConfig);
		logger.info("Reset sensor device");
		resetDevice();
	}
	
	public void parseMessage(String in) {
		byte[] inByte = in.getBytes();
		for (int i =0; i < inByte.length; i++) {
			char c = (char)inByte[i];
			if (c == '\n' || c == '\r') {
				buf.rewind();
				String message = buf.toString().trim();
				splitByDelimiter(message);
				buf.clear();
			} else if (c >= 32 && c <= 125 && buf.hasRemaining()){
				buf.put(c);
				//System.out.print(c);
			}
		}
	}
	
	public void splitByDelimiter(String data) {
		if (data != null) {
			String[] da = data.split("\\|");
			for (String token : da) {
				String[] kv = token.split("=");
				if (kv.length == 2) {
					if ("ON".equals(kv[0])) {

					} else if ("P".equals(kv[0])) {
						try {
						pitch = Integer.parseInt(kv[1]);
						} catch (NumberFormatException e) {}
					} else if ("R".equals(kv[0])) {
						try {
							roll = Integer.parseInt(kv[1]);
						} catch (NumberFormatException e) {}
					} else if ("Y".equals(kv[0])) {
						try {
							yaw = Integer.parseInt(kv[1]);
						} catch (NumberFormatException e) {}
					} else if ("AP".equals(kv[0])) {
					} else if ("OUT".equals(kv[0].trim())) {
						logger.info(kv[1]);
					} else {
						logger.log(Level.WARNING, "Unknown key " + kv[0]);
					}
				} else {
					//logger.log(Level.WARNING, "Unknown serial message " + data);
				}
			}
		}
	}
	
	public void stop() {
		if (serial != null && serial.isOpen()) {
			try {
				serial.close();
			} catch (Exception e) {
				logger.log(Level.SEVERE, "Error during close serial", e);
			}
		}
	}
	
	public void turretUp(int value) {
		try {
			serial.write((byte)'u');
			serial.write((byte)value);
			serial.write((byte)10);
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Error during reset sensor", e);
		}
	}
	
	public void turretDown(int value) {
		try {
			serial.write((byte)'d');
			serial.write((byte)value);
			serial.write((byte)10);
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Error during reset sensor", e);
		}
	}
	
	public void resetDevice() {
		try {
			serial.write((byte)'r');
			serial.write((byte)10);
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Error during reset sensor", e);
		}
	}

	public int getPitch() {
		return pitch;
	}

	public int getRoll() {
		return roll;
	}
	
	public int getYaw() {
		return yaw;
	}
	
}
