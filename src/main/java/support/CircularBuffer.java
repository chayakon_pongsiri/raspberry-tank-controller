package support;


public class CircularBuffer {

	private static int DEFULT_SIZE = 5;
	
	private int[] buffer;
	private int size;
	private int position = 0;
	
	public CircularBuffer() {
		this(DEFULT_SIZE);
	}
	
	public CircularBuffer(int size) {
		this.size = size;
		this.buffer = new int[size];
	}
	
	public void add(int data){
		if (position > size - 1) {
			position = 0;
		}
		buffer[position++] = data;
	}
	
	public int[] getBuffer() {
		return this.buffer;
	}
	
	public void fill(int value) {
		for (int i = 0; i < this.buffer.length; i++) {
			this.buffer[i] = value;
		}
	}
	
	public int getAvg() {
		int sum = 0;
		for (int i = 0; i < this.buffer.length; i++) {
			int value = this.buffer[i];
			//if (value <= 0) {
			//	continue;
			//}
			sum += value;
		}
		return  (sum/this.buffer.length);
	}
	
	public int getMax() {
		int max = 0;
		for (int i = 0; i < this.buffer.length; i++) {
			int value = this.buffer[i];
			if (value > max) {
				max = value;
			}
		}
		return  max;
	}
	
	public void printBuffer() {
		for (int i = 0; i < this.buffer.length; i++) {
			System.out.print(this.buffer[i]);
			System.out.print(",");
		}
		System.out.println();
	}
	
	public int size() {
		return this.size;
	}
}
