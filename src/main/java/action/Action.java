package action;

public class Action {

	private String name;
	private long delay;
	private int speed;
	
	public Action(String name) {
		this.name = name;
	}
	
	public Action(String name, long delay) {
		this.name = name;
		this.delay = delay;
	}
	
	public Action(String name, int speed, long delay) {
		this.name = name;
		this.speed = speed;
		this.delay = delay;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getDelay() {
		return delay;
	}

	public void setDelay(long delay) {
		this.delay = delay;
	}

	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	@Override
	public String toString() {
		return "Action [name=" + name + ", delay=" + delay + ", speed=" + speed + "]";
	}

	
}
