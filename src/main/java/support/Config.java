package support;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class Config {

	static Config instance = null;
	Properties properties = new Properties();
	
	public static Config getInstance() {
		if (instance == null) {
			instance = new Config();
		}
		return instance;
	}
	
	private Config() {
		
	}
	
	public void load(String config) throws FileNotFoundException, IOException {
		properties.load(new FileReader(new File(config)));
	}
	
	public String get(String key) {
		return properties.getProperty(key);
	}
	
	public String get(String key, String defaultValue) {
		return properties.getProperty(key, defaultValue);
	}
	
	public int getInt(String key) {
		return Integer.parseInt(properties.getProperty(key));
	}
	
	public int getInt(String key, String defaultValue) {
		return Integer.parseInt(properties.getProperty(key, defaultValue));
	}
	
	public double getDouble(String key) {
		return Double.parseDouble(properties.getProperty(key));
	}
	
	public double getDouble(String key, String defaultValue) {
		return Double.parseDouble(properties.getProperty(key, defaultValue));
	}
}
