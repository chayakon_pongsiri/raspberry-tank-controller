package support;

import static support.ValueMapping.*;

public class JoyStick {

	int xmin;
	int xmax;
	int ymin;
	int ymax;
	int error;
	
	private int x;
	private int y;
	
	private int xPercent;
	private int yPercent;
	
	public JoyStick(int xmin, int xmax, int ymin, int ymax, int error) {
		this.xmin = xmin;
		this.xmax = xmax;
		this.ymin = ymin;
		this.ymax = ymax;
		this.error = error;
	}
	
	public void update(int x, int y) {
		this.x = x;
		this.y = y;
		this.xPercent = mapValueToPercent(x, xmin, xmax);
		this.yPercent = mapValueToPercent(y, ymin, ymax);
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public int getXPercentFromCenter() {
		if (xPercent >= 50) {
			return mapValueToPercent(xPercent, 50 + error, 100);
		} else {
			return 100 - mapValueToPercent(xPercent, 0, 50 - error);
		}
	}
	
	public int getYPercentFromCenter() {
		if (yPercent >= 50) {
			return mapValueToPercent(yPercent, 50 + error, 100);
		} else {
			return 100 - mapValueToPercent(yPercent, 0, 50 - error);
		}
	}
	
	public boolean isMoveLeft() {
		return xPercent < 50;
	}
	
	public boolean isMoveRight() {
		return xPercent > 50;
	}
	
	public boolean isMoveUp() {
		return yPercent > 50;
	}
	
	public boolean isMoveDown() {
		return yPercent < 50;
	}
	
	public boolean isXMove() {
		return notBetween(xPercent, 50, error);
	}
	
	public boolean isYMove() {
		return notBetween(yPercent, 50, error);
	}
	
	public boolean isXNearPercent(int percent) {
		return between(xPercent, percent, error);
	}
	
	public boolean isYNearPercent(int percent) {
		return between(yPercent, percent, error);
	}
	
	public boolean isXNearCenter() {
		return between(xPercent, 50, error);
	}
	
	public boolean isYNearCenter() {
		return between(yPercent, 50, error);
	}
	
	public boolean isXYNearCenter() {
		return between(xPercent, 50, error) && between(yPercent, 50, error);
	}

	public int getXPercent() {
		return xPercent;
	}
	
	public int getYPercent() {
		return yPercent;
	}
	
}
